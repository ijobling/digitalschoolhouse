// prerequsites
// npm install webpack
// npm install style-loader css-loader --save
// npm install script-loader css-loader --save

module.exports = {
  entry: './libs/codio-tutorial.js',
  output: {
    path: __dirname,
    filename: './libs/codio-tutorial-bundle.js'
  },
  externals: {
    'blockly': 'Blockly'
  }
};
