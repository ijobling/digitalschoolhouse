var game = null;

var forceX = 0;
var forceY = 0;
var gravity = 10;
var mass = 0;
var drag = 0;
var debug = true;
var isTest = false;

function fire() {
  var gravityClamped = gravity;
  if (gravityClamped < -12)
    gravityClamped = -12;
  if (gravityClamped > 12)
    gravityClamped = 12;
  game.setWorldGravity(0, gravityClamped);
  game.setRocketDrag(drag);
  game.setRocketMass(mass);
  game.shoot(forceX, -forceY);
}

(function () {
  var contentCanvas;
  var contentCanvasDefaultWidth;
  var contentCanvasDefaultHeight;
  var debugCanvas;

  function resizeCanvas() {
     if(contentCanvas) {
      var canvasHeight = $(window).height() - 50;
      if(codioTutorial.isUseBlockly()) {
        canvasHeight -= $('#codePartition').height();
      }
      if (canvasHeight > contentCanvasDefaultHeight)
          canvasHeight = contentCanvasDefaultHeight;
      $(contentCanvas).css("height", canvasHeight);
      //$('<style>#contentCanvas { height: ' + canvasHeight + 'px; }</style>').appendTo('head');
    }
  }

  function stopRocketGame() {
    if (game != null) {
      game.destroy();
    }
  }

  function startRocketGame(callback) {
    stopRocketGame();
    game = new RocketGame(
      contentCanvas,
      debugCanvas,
      callback);
  };

  codioTutorial.readyEvent.addListener(function () {
    contentCanvas = document.getElementById('contentCanvas');
    debugCanvas = document.getElementById('debugCanvas');

    contentCanvasDefaultWidth = contentCanvas.width;
    contentCanvasDefaultHeight = contentCanvas.height;
    $(window).resize(function() {
      resizeCanvas();
    });
    resizeCanvas();

    $('#content').show();

    if (!codioTutorial.isUseBlockly()) {
      codioTutorial.loadUserScript(function () {
        startRocketGame(function () {
          codioTutorial.executeScript();
        });
      });
    }
    else {
      startRocketGame();
    }
  });

  codioTutorial.scriptExecuteEvent.addListener(function () {
    if (!codioTutorial.isUseBlockly()) {
      codioTutorial.loadUserScript(function () {
        startRocketGame(function () {
          codioTutorial.executeScript();
        });
      });
    }
    else {
      startRocketGame(function () {
        codioTutorial.executeScript();
      });
    }
  });

  codioTutorial.scriptCompleteEvent.addListener(function () {
    if (isTest) {
      codioTutorial.sendResults({
        forceX: forceX,
        forceY: forceY,
        gravity: gravity,
        mass: mass,
        drag: drag
      });
      socket.emit('reloaded');
    }
  });

  codioTutorial.errorEvent.addListener(function (err) {
    swal('Error', err.message);
    if (isTest) {
      log('Cycle abort');
      socket.emit('test-abort');
      return;
    }
  });

  /******************************/
  /* Socket.io events for tests */
  /******************************/

  // Console logging
  function log(msg) {
    if (debug) {
      console.log(msg);
    }
  }

  // Socket.io call to re-test
  var url =
      "https://" +
      new URL(location.href).hostname +
      ":9500";
  socket = io(url);

  socket.on('error', function(err) {
    log("Socket Error : " + JSON.stringify(err));
  });

  socket.on('connect', function(err) {
    log('-------------------\nConnection made');
  });

  socket.on('disconnect', function(err) {
    log('Disconnected');
  });

  socket.on('client-reload', function(data){
    log('.................\nClient reload request');
    isTest = true;
    codioTutorial.dispatchExecute();
  });
}());

(function() {
  RocketGame = (function() {
    var PIXELS_PER_METER = 30;
    var gravityX = 0;
    var gravityY = 10;
    var frameRate = 30;
    var forceMultiplier = 5;
    var rocket, world;
    function RocketGame(canvas, debugCanvas, loadCallback) {
      var blockHeight;
      var blockWidth;
      var ghost;
      var ground;
      var groundLevelPixels;
      var i, j;
      var initRocketXPixels;
      var leftPyamid;
      var levels;
      var myBlock;
      var topOfPyramid;
      var worldHeightPixels;
      var worldWidthPixels;
      var x, y;
      var _i, _j;
      var _ref;
      world = new EaselBoxWorld(
        this,
        frameRate,
        canvas,
        debugCanvas,
        gravityX,
        gravityY,
        PIXELS_PER_METER);
      //stats = new Stats();
      //statsCanvas.appendChild(stats.domElement);
      worldWidthPixels = canvas.width;
      worldHeightPixels = canvas.height;
      initRocketXPixels = 100;
      groundLevelPixels = worldHeightPixels - (37 / 2);
      world.addImage('rocket/img/sky.jpg', {
        scaleX: 1.3,
        scaleY: 1.3,
        x: -80
      });
      ground = world.addEntity({
        widthPixels: 2024,
        heightPixels: 37,
        imgSrc: 'rocket/img/ground-cropped.png',
        type: 'static',
        xPixels: 0,
        yPixels: groundLevelPixels
      });
      var offset_x = 130;
      var robot_x = initRocketXPixels - 110;
      var robot = world.addImage('rocket/img/robot.png', {
        x: robot_x - offset_x,
        y: worldHeightPixels - 240
      });
      var rocket_x = initRocketXPixels - 26;
      rocket = world.addEntity({
        widthPixels: 60,
        heightPixels: 35,
        imgSrc: 'rocket/img/rocket.png',
        type: 'static',
        xPixels: rocket_x - offset_x,
        yPixels: groundLevelPixels - 170
      });
      rocket.physicsDisable();
      rocket.selected = false;
      if (loadCallback) {
        world.setOnLoadBitmapsComplete(function () {
          TweenLite.to(robot, 0.8, {x:robot_x});
          TweenLite.to(rocket.easelObj, 0.8, {
            x:rocket_x,
            onComplete:function () {
              rocket.physicsEnable();
                loadCallback();
            }
          });
        });
      }
      blockWidth = 15;
      blockHeight = 60;
      leftPyamid = 440;
      levels = 3;
      topOfPyramid = groundLevelPixels - levels * (blockHeight + blockWidth) + 26;
      for (i = _i = 0; 0 <= levels ? _i < levels : _i > levels; i = 0 <= levels ? ++_i : --_i) {
        for (j = _j = 0, _ref = i + 1; 0 <= _ref ? _j <= _ref : _j >= _ref; j = 0 <= _ref ? ++_j : --_j) {
          x = leftPyamid + (j - i / 2) * blockHeight;
          y = topOfPyramid + i * (blockHeight + blockWidth);
          myBlock = world.addEntity({
            widthPixels: blockWidth,
            heightPixels: blockHeight,
            imgSrc: 'rocket/img/block1_15x60.png',
            xPixels: x,
            yPixels: y
          });
          if (j <= i) {
            myBlock = world.addEntity({
              widthPixels: blockWidth,
              heightPixels: blockHeight,
              imgSrc: 'rocket/img/block1_15x60.png',
              xPixels: x + blockHeight / 2,
              yPixels: y - (blockHeight + blockWidth) / 2,
              angleDegrees: 90
            });
            ghost = world.addEntity({
              widthPixels: 30,
              heightPixels: 36,
              imgSrc: 'rocket/img/ghost_30x36.png',
              xPixels: x + (blockHeight / 2),
              yPixels: y + 11
            });
          }
        }
      }
    }
    RocketGame.prototype.tick = function() {
      //return stats.update();
    };
    RocketGame.prototype.destroy = function() {
      return world.destroy();
    };
    RocketGame.prototype.shoot = function(forceX, forceY) {
      rocket.selected = false;
      rocket.setType('dynamic');
      rocket.body.ApplyImpulse(
        world.vector(forceX, forceY),
        world.vector(rocket.body.GetPosition().x,
        rocket.body.GetPosition().y));
    };
    RocketGame.prototype.setRocketMass = function(mass) {
      rocket.body.SetMassData({mass:mass});
    };
    RocketGame.prototype.setRocketDrag = function(drag) {
      rocket.body.SetLinearDamping(drag);
    };
    RocketGame.prototype.setWorldGravity = function(x,y) {
      world.box2dWorld.SetGravity(
        new Box2D.Common.Math.b2Vec2(x, y));
    }
    return RocketGame;
  })();
}).call(this);

//@ sourceURL=rocketgame.js
