if (Blockly) {
  Blockly.Blocks['event_shoot'] = {
    init: function() {
      this.setColour(20);
      this.appendValueInput("forceX")
          .setCheck("Number")
          .appendField("forceX");
      this.appendValueInput("forceY")
          .setCheck("Number")
          .appendField("forceY");
      this.setInputsInline(true);
      this.setPreviousStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['event_shoot'] = function(block) {
    var value_forceX = Blockly.JavaScript.valueToCode(block, 'forceX', Blockly.JavaScript.ORDER_ATOMIC);
    var value_forceY = Blockly.JavaScript.valueToCode(block, 'forceY', Blockly.JavaScript.ORDER_ATOMIC);
    value_forceX = value_forceX === '' ? 0 : value_forceX;
    value_forceY = value_forceY === '' ? 0 : value_forceY;
    var code = 'game.shoot('+value_forceX+', '+value_forceY+');\n';
    return code;
  };

  Blockly.Blocks['horizontal_force_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Horizontal Force');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['10', '10'], ['20', '20'],['50', '50'], ['100', '100'], ['500', '500']]);
      input.appendField(dropdown, 'horizontalForce');
    }
  };
  Blockly.JavaScript['horizontal_force_fixed'] = function(block) {
    var value_forceX = block.getFieldValue('horizontalForce');
    var code = 'forceX = '+value_forceX+';\n';
    return code;
  };

  Blockly.Blocks['horizontal_force_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("horizontal_force_variable")
          .appendField("Set Horizonal Force");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['horizontal_force_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'horizontal_force_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code = 'forceX = '+value+';\n';
    return code;
  };

  Blockly.Blocks['vertical_force_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Vertical Force');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['5', '5'], ['15', '15'], ['30', '30'], ['-5', '-5'], ['-15', '-15'], ['-30', '-30']]);
      input.appendField(dropdown, 'verticalForce');
    }
  };
  Blockly.JavaScript['vertical_force_fixed'] = function(block) {
    var value_forceY = block.getFieldValue('verticalForce');
    var code = 'forceY = '+value_forceY+';\n';
    return code;
  };

  Blockly.Blocks['vertical_force_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("vertical_force_variable")
          .appendField("Set Vertical Force");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['vertical_force_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'vertical_force_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code =  'forceY = '+value+';\n';
    return code;
  };

  Blockly.Blocks['fire_basic'] = {
    init: function() {
      this.setColour(20);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Fire');
    }
  };
  Blockly.JavaScript['fire_basic'] = function(block) {
    var code = 'fire();\n';
    return code;
  };

  Blockly.Blocks['fire_fixed'] = {
    init: function() {
      this.setColour(20);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Fire');
    }
  };

  Blockly.JavaScript['fire_fixed'] = function(block) {
    var code = 'fire(100, 0);\n';
    return code;
  };

  Blockly.Blocks['gravity_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Gravity');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['2', '2'], ['5', '5'], ['10', '10']]);
      input.appendField(dropdown, 'gravityForce');
    }
  };
  Blockly.JavaScript['gravity_fixed'] = function(block) {
    var value = block.getFieldValue('gravityForce');
    var code = 'gravity = '+value+';\n';
    return code;
  };

  Blockly.Blocks['gravity_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("gravity_variable")
          .appendField("Set Gravity");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['gravity_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'gravity_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code = 'gravity = '+value+';\n';
    return code;
  };

  Blockly.Blocks['drag_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Drag');
      var dropdown = new Blockly.FieldDropdown([['0', '0'], ['5', '5'], ['10', '10']]);
      input.appendField(dropdown, 'drag');
    }
  };
  Blockly.JavaScript['drag_fixed'] = function(block) {
    var value = block.getFieldValue('drag');
    var code = 'drag = '+value+';\n';
    return code;
  };

  Blockly.Blocks['drag_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("drag_variable")
          .appendField("Set Drag");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['drag_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'drag_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code = 'drag = '+value+';\n';
    return code;
  };

  Blockly.Blocks['mass_fixed'] = {
    init: function() {
      this.setColour(65);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      var input = this.appendDummyInput();
      input.appendField('Set Mass');
      var dropdown = new Blockly.FieldDropdown([['1', '1'], ['5', '5'], ['20', '20']]);
      input.appendField(dropdown, 'mass');
    }
  };
  Blockly.JavaScript['mass_fixed'] = function(block) {
    var value = block.getFieldValue('mass');
    var code = 'mass = '+value+';\n';
    return code;
  };

  Blockly.Blocks['mass_variable'] = {
    init: function() {
      this.setColour(65);
      this.appendValueInput("mass_variable")
          .appendField("Set Mass");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['mass_variable'] = function(block) {
    var value = Blockly.JavaScript.valueToCode(block, 'mass_variable', Blockly.JavaScript.ORDER_ATOMIC);
    value = value === '' ? 0 : value;
    var code = 'mass = '+value+';\n';
    return code;
  };

  Blockly.Blocks['rocket_variable'] = {
    init: function() {
      this.setColour(65);
      var textInput = new Blockly.FieldTextInput('');
      this.appendValueInput("value")
          .appendField('Set', 'value')
          .appendField(textInput, 'var')
          .setCheck("Number");
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
  Blockly.JavaScript['rocket_variable'] = function(block) {
    var value_var = block.getFieldValue('var').toLowerCase();
    var value = Blockly.JavaScript.valueToCode(block, 'value', Blockly.JavaScript.ORDER_ATOMIC);

    var code = '';
    switch(value_var){
      case 'gravity':
        code = 'gravity = '+value+';\n';
        break;
      case 'drag':
        code = 'drag = '+value+';\n';
        break;
      case 'mass':
        code = 'mass = '+value+';\n';
        break;
      case 'forcex':
        code = 'forceX = '+value+';\n';
        break;
      case 'forcey':
        code = 'forceY = '+value+';\n';
        break;
    }
    return code;
  };
}

//@ sourceURL=rocketgame-customblockly.js
