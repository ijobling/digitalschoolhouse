 /**
 * Object that during their initialization can call this function
 * This will extend the calling object with basic
 * Event Dispatching functionality
 *
 */
function EventDispatcher() {
  this._listeners = [];
  this.isEventDispatcher = true;
}

EventDispatcher.prototype = {
  dispatchEvent: function(eventObject) {
    for (var i = 0; i < this._listeners.length; i++) {
      this._listeners[i](eventObject);
    }
  },
  removeListener: function(callback) {
    for (var i = 0; i < this._listeners.length; i++) {
      var listener = this._listeners[i];
      if (listener === callback) {
        this._listeners.splice(i, 1);
      }
    }
  },
  addListener: function(callback) {
    this._listeners.push(callback);
  }
}
