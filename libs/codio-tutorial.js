require('!style!css!./swal/sweet-alert.css');

require('script!./async.js');
require('script!./event_dispatcher.js');
require('script!./swal/sweet-alert.min.js');
require('script!./jquery-2.1.3.min.js');
require('script!./jquery-urlparam.js');
require('script!./jquery-config.js');
require('script!./socket.io-1.3.5.js');

var Blockly = !Blockly ? require('blockly') : Blockly;

/**
 * Initalize tutorial
 */

window.codioTutorial = (function () {
  var useBlockly = false;

  var readyEvent = new EventDispatcher();
  var scriptExecuteEvent = new EventDispatcher();
  var scriptCompleteEvent = new EventDispatcher();
  var errorEvent = new EventDispatcher();
  var loadCompleteEvent = new EventDispatcher();
  var executeError = false;

  // Get options.
  var tutorialName = $.urlParam('tutorial');

  // Script defined by user
  var userScript = '';

  function showRunCodeButton(value) {
    if (value) {
      $('#runJS').show();
    }
    else {
      $('#runJS').hide();
    }
  }

  function showShowCodeButton(value) {
    if (value) {
      $('#showJS').show();
    }
    else {
      $('#showJS').hide();
    }
  }

   /**
    * Eval either blockly or user script
    * @param {Function} On complete callback.
    */
  function executeScript(callback) {
    if (useBlockly) {
      executeBlocklyScript(callback);
    }
    else {
      executeUserScript(callback);
    }
  }

   /**
    * Eval script set in URL param 'script'
    * @param {Function} On complete callback.
    */
  function executeUserScript(callback) {
    if (userScript != '') {
      evalScript(userScript);
      if (callback) {
        callback();
      }
    }
  }

   /**
    * Generate and run Javascript from Blockly
    */
  function executeBlocklyScript(callback) {
    LoopTrap = 1000;
    Blockly.JavaScript.INFINITE_LOOP_TRAP =
      'if (--LoopTrap == 0) throw "Infinite loop.";\n';
    var code = Blockly.JavaScript.workspaceToCode();
    Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
    evalScript(code);
    if (callback) {
      callback();
    }
  }

   /**
    * Generate Javascript from Blockly
    */
  function getBlocklyScript() {
    Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
    return Blockly.JavaScript.workspaceToCode();
  }

  /**
   * Dynamically load and run a user defined .js file set
   * in URL parameter 'script'
   * @param {Function} On complete callback.
   */
  function loadUserScript(callback) {
    var scriptFile = $.urlParam('script');
    if (!scriptFile && !useBlockly) scriptFile = 'user-01.js';
    var url =
      'https://' +
      new URL(location.href).hostname +
      '/' + codioTutorialDefinition.dir +
      '/' + scriptFile;
    if(scriptFile != undefined) {
      $.ajax({
        async: true,
        type: 'GET',
        url: url,
        data: '',
        dataType: 'text',
        complete: loadScriptComplete,
        success: function (data) {
          userScript = data;
          if(callback) {
            callback();
          }
        }
      });
    }
  }

  /**
   * Send test results to Codio.
   * @param {Object} Any object.
   */
  function sendResults(results) {
    var url =
      'https://' +
      new URL(location.href).hostname +
      ':9500/test-data';
    xmlHttp = new XMLHttpRequest();
    xmlHttp.open('POST', url, false);
    try{
      xmlHttp.send(JSON.stringify(results));
    }
    catch(e) {
      console.log(e);
    }
    if(xmlHttp.responseText != 'ok') {
      dispatchError({
        type: 'error',
        mode: 'jqXHRerror',
        message: 'Problem sending in the test results!'
      });
    }
  }

  /**
   * When document is ready, load relevant tutorial XML, HTML and userScript. Show
   * and initalize blockly if specified by URL parameters.
   */
  $(document).ready(function() {
    var tutorial = codioTutorialDefinition;
    useBlockly = tutorial.blockly;
    if (!tutorial) {
      var message =
        'No Tutorial ' +
        tutorialName +
        'defined';
      dispatchError({
        type: 'error',
        mode: 'init',
        message: message
      });
      return;
    }
    if (!tutorial.html) {
      var message =
        'No Tutorial HTML defined for: ' +
        tutorialName;
      dispatchError({
        type: 'error',
        mode: 'init',
        message: message
      });
      return;
    }
    $('#content').load(tutorial.html, function () {
      if (useBlockly) {
        $('#codePartition').show();
        $('#codePartition').css('height', '50%');
        $('#contentPartition').css('height', '50%');
        if (!tutorial.xml) {
          var message =
            'No Tutorial XML defined for: ' +
            tutorialName;
          dispatchError({
            type: 'error',
            mode: 'init',
            message: message
          });
          return;
        }
        loadXMLDoc(tutorial.xml, function (tutorialXML) {
          initBlockly(tutorialXML);
          readyEvent.dispatchEvent({
            type: 'ready'
          });
        });
      }
      else {
        loadUserScript(function () {
          readyEvent.dispatchEvent({
            type: 'ready'
          });
        });
      }
    });
  });

  /**
   * Load a list of .js files.
   * @param {Array} Script URLs.
   * @param {Function} All scritps have been loaded.
   */
  function loadScripts(scriptURLs, callback) {
    if (!scriptURLs) {
      callback();
    }
    var load = [];
    scriptURLs.forEach(function (url) {
      load.push(function (complete) {
        $.getScript(url, function(data, textStatus, jqxhr) {
          console.log('Got script: ' + url);
          complete();
        });
      });
    })
    async.series(load, callback);
  }

   /**
    * Eval code string
    * @param {String} Code to execute.
    */
  function evalScript(code) {
    try {
      executeError = false;
      eval(code);
      scriptCompleteEvent.dispatchEvent({
        type: 'complete'
      });
    }
    catch (e) {
      executeError = true;
      console.log(e);
      errorEvent.dispatchEvent({
        type: 'error',
        mode: 'script',
        message: e.message,
        error: e
      });
    }
  }

  /**
   * Initialize Blockly if 'blockly' URL parameter exits.
   * @param {Object} XML object defining the tutorial content.
   */
  function initBlockly(tutorialXML) {
    if (!useBlockly) return;
    var blocklyArea = document.getElementById(
      'blockly-area');
    var startBlocks = tutorialXML.getElementsByTagName(
      'startBlocks')[0];
    var toolboxLabelSize = tutorialXML.getElementsByTagName(
      'toolboxLabelSize')[0].firstChild.data;
    var toolbox = tutorialXML.getElementsByTagName(
      'toolbox')[0];
    toolbox = toolboxLabelSize !== 0 ? toolbox : '';
    Blockly.inject(blocklyArea, {
      toolbox: toolbox
    });
    Blockly.Xml.domToWorkspace(
      Blockly.mainWorkspace,
      startBlocks);
    updateToolboxLabel(
      toolboxLabelSize);
  }

  /**
   * Resize Blockly labels.
   * @param {Number} Width in pixels of Toolbox label.
   */
  function updateToolboxLabel(value) {
    document.getElementById('toolboxLabel').style.width =
      value + 'px';
    document.getElementById('instructionsLabel').style.width =
      'calc(100% - ' + value + 'px)';
  }

  /**
   * Ajax On Complete for loadScript
   */
  function loadScriptComplete(jqXHR, error) {
    if(jqXHR.status == 404) {
      var message = 'Problem fetching the script. Contact support@codio.com.';
      dispatchError({
        type: 'error',
        mode: 'jqXHRerror',
        message: message,
        error: error
      });
    }
    else {
      loadCompleteEvent.dispatchEvent({
        type: 'loaded'
      });
    }
  }

  /**
   * Error dispach shorthand.
   */
  function dispatchError(err) {
    errorEvent.dispatchEvent(err);
  }

  function dispatchExecute() {
    scriptExecuteEvent.dispatchEvent({
      type: 'execute'
    });
  }

  /**
   * Load an XML file.
   * @param {String} File path.
   * @return {Object} Parsed XML.
   */
  function loadXMLDoc(filename, callback) {
    $.ajax({
      type: 'GET',
      url: filename,
      dataType: 'xml',
      success: function (xml) {
        callback(xml);
      }
    });
  }

 function isUseBlockly() {
   return useBlockly;
 }

  var exports = {
    'name': tutorialName,
    'isUseBlockly': isUseBlockly,
    'showRunCodeButton': showRunCodeButton,
    'showShowCodeButton': showShowCodeButton,
    'readyEvent': readyEvent,
    'loadCompleteEvent': loadCompleteEvent,
    'dispatchExecute': dispatchExecute,
    'scriptExecuteEvent': scriptExecuteEvent,
    'scriptCompleteEvent': scriptCompleteEvent,
    'errorEvent': errorEvent,
    'loadUserScript': loadUserScript,
    'executeScript': executeScript,
    'executeUserScript': executeUserScript,
    'executeBlocklyScript': executeBlocklyScript,
    'getBlocklyScript': getBlocklyScript,
    'sendResults': sendResults
  };

  return exports;
}());

/**
 * Dispatches events for the 'showCode' button.
 */
window.showBlocklyCodeButtonClick = function() {
  var code = codioTutorial.getBlocklyScript();
  code = code.split('\n').join('<br>');
  swal({
    title: 'JavaScript Code',
    text: '<div style="text-align: left; display: block;">'+code+'</div>',
    html: true
  });
}

/**
 * Dispatches events for the 'runCode' button.
 */
window.runBlocklyCodeButtonClick = function() {
  codioTutorial.dispatchExecute();
}
