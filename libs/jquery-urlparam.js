/**
 * Parse a URL parameter.
 * @param {String} variable name
 * @return {String} value
 */
$.urlParam = function(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(location.href);
  if (results==null){
     return null;
  }
  else{
     return results[1] || 0;
  }
}
