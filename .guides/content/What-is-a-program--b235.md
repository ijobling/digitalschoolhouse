We are going to use a couple of games to introduce us to new coding concepts.

|||definition
## What is a program?
A program is a set of instructions that are executed one after the other. Once the last instruction has been executed, the program finishes.
|||

On the left hand side you can see a game. This game has several instructions that are executed one by one and in order.

1. Horizontal Force : the speed with which the bird is propelled sideways.
1. Vertical Force : the speed with which the bird is propelled upwards.
1. Fire the bird!

|||info
## Instructions
At this stage, it's very simple. Simply press the **Run it!** button and you will see what happens.

Notice that the rocket simply drops to the ground! That's because you need to set the horizontal force to be something other than 0. Try changing the Horizontal and Vertical Force values.
|||
