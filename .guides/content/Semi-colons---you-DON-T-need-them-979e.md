You may have heard that you have to use semi-colons everywhere. It's not true and we advocate not using them. Huge amounts of Codio itself are written in Javascript and we don't use them.

There are, in fact, 2 special cases where you do require them but we have not yet encountered them in tens of thousands of lines of code. So, you're not likely to encounter them for a long time.