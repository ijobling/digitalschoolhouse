In the left hand side you can see a new tab open up with some Javascript code.

It should be pretty obvious what these 4 instructions are doing but let's explain some Javascript.

|||info
1. `gravity`, `forceX` and `forceY` are variables. 
2. `=` means "assign the value after the `=` to the variable, so `gravity` is assigned a value of 5 etc.
3. The final instruction is `fire()`. `fire` is the name of a *function* and the  `()` is the bit that tells Javascript it is a function.

|||

## So what is a function?
We'll explain functions in more details later on. For now, just think of a function a command that executes one more more instructions (in this case, it fires the rocket).



