You have hopefully noticed that many of the things we have been playing around with have names and values.

For instance, **Horizontal Force** might have a value of **20**.

Horizontal Force is actually called a variable.

|||definition
## Variables
A variable is a named placeholder for a value, where that value can change or *vary* (that's why it's called a variable, because the value can vary).
|||

## What variables have we created so far?
These are the variables we've been working with so far.

1. **Gravity** : the strength of [gravity]().
1. **Drag** : the *draginess* as it flies through the air.
1. **Vertical Force** : the speed with which the bird is propelled upwards.
1. **Horizontal Force** : the speed with which the bird is propelled sideways.

Variables are everywhere in coding and we will be constantly playing around with them. Don't worry about the terminology for now. You'll soon become comfortable with them.

