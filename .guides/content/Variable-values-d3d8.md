Let's now look at another way we can work with variables. In the latest version of the game, we now have a new toolbox. We've got all the same instructions as before, but this time you'll notice there is a new one.

![](.guides/img/num-plug.png)

This is simply a place you can set a value. Drag it into the Code Area and you will see you can enter a value into it.

Here's how you work with it.

1. Drag in the Horizontal Force instruction.
1. Now drag in the Value block.
1. Enter a value `20` into it.
1. To assign the value (`20` or whatever you entered) to the variable, you simply drag the value into the socket of the variable. It should look like this.
1. Don't forget to add the Fire command.
1. Press the Run it! button to run the game.

![](.guides/img/var-plug-num.png)

To create more values in the Code Area, you can do either of the following...

1. Drag a new value into the Code Area from the Toolbox.
1. Click on a value block already in the Code Area and copy it using a) if you're using a PC, press Ctrl+C (to copy) then Ctrl+V (to paste) or b) if you're using a Mac press Cmd+C (to copy) then Cmd+V (to paste).

## Play
Go ahead and play a few variables and values. Don't forget to add the **Fire** instructions and fire the bird.