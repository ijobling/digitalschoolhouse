|||challenge
Make the following settings and press **Fire**.

1. Gravity = 10
1. Drag = 0
1. Horizontal Force = 50
1. Make sure the **Fire** instruction is there

When you've done this, press the button below to check you've got it right.

{Check it!!}(node /home/codio/workspace/.guides/tests/game-tests.js 1)

|||

