Go ahead and play with some values. The full set of variables at your disposal are

- gravity
- forceX
- forceY
- drag

Don't forget, everything in Javascript is case-sensitive, so check your code if it is not doing what you expect.

