We have introduced a few concepts here that you should try to get comfortable with

1. Semi Colons (instructions should end with `;`).
1. Comments (lines starting with `//`).
1. Case sensitivity (`forceX` is not the same as `forceY`).

That's it for this unit.
