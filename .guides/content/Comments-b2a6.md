Something else you will see a lot in code is comments. 

Lines which are comments start with `//`.

Comments are good! Very often, other people will want to look at your code. Comments explain to others important bits of your code.

If fact, they are very useful to you if you ever come back to look at your code and can't remember how it works.

In summary, add lots of comments to your code.