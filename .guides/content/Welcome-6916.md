Welcome to the Digital Schoolhouse demo. Everything you see here is a prototype for our full set of "An introduction to coding" content.

We've got a couple of games to keep you entertained which we'll do some coding on.

## Angry Rockets
![](.guides/img/rocket-game.jpg)

## Fairytale
![](.guides/img/castle-game.jpg)

