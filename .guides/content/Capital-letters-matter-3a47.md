One thing you should be very aware of is *case sensitivity*.

- Capital letters (X) are called *upper case*.
- ordinary letters (x) are called *lower case*.

|||definition
Javascript is sensitive to upper and lower case, which is why we use the expression *case sensitive*
|||

So, looking at our code

- `forcex` is not the same as `forceX`. `forcex` is seen as a completely different variable.
- `fire()` works, but `Fire()` would not work.

Feel free to try it out.