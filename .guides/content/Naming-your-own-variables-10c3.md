We are now going to take the last step with variables and actually create our own variables with any name we choose.

|||info
It is important that you actually use the right variable names, or the game won't work properly.

For example, the variable name for **Horizonal Speed** is **forceX**. If you were to use **Xforce**, it would not work.
|||

Here are the full set of variable names for our game. Don't use capital letters or spaces, it has to be exactly as you see it. When we come to text based programming languages, you'll see why.

**gravity**
**drag**
**forceX**
**forceY**

Make sure you press the Run It! button to launch the game.
