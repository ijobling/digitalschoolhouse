In the previous section, the instructions were created for you. This time, you are going add your own instructions so you can control everything yourself.


1. The strength of [gravity](https://www.youtube.com/watch?v=MTY1Kje0yLg)
1. The *draginess* as it flies through the air.
1. Vertical Force : the speed with which the bird is propelled upwards.
1. Horizontal Force : the speed with which the bird is propelled sideways.

## Have a play
1. Change the **Gravity** value to something other than 0.
1. Fire the bird and see what happens.
1. Add in the other commands, one by one by dragging in from the Toolbox area to the Code Instructions area.
1. If you drag in new commands from the toolbar, make sure you click them into place before the **Fire** command.
1. Play with the values next to each of the instructions and see what effect it has when you fire the bird.

Go ahead and have some fun playing with the values and see if you can really smash things up!

|||info
You can add the same command twice right above each other but with different values. Notice that the last command is the one that wins as it overwrites whatever was there before.

Also, if you add a command *after* the **Fire** command, it will not have any effect as the rocket will already have been launched.
|||


