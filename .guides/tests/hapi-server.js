//npm install underscore

var _ = require('underscore');
var url = require('url');
var http = require('http');
var fs = require('fs');
var index = fs.readFileSync('index.html');
var indexTemplate = _.template(''+index);
var Hapi = require('hapi');

var libURLProd = 'https://d6wn7dyfyjped.cloudfront.net/v1/';
var libURLTest = 'libs/';

var gResult;

var easelBox2DLibs = [
    'stats.js',
    'easel.js',
    'Box2dWeb-2.1.a.3.min.js',
    'easelBox.js',
    'greensock/TweenMax.min.js'
];

var rocketGameJS = [
    'rocket/js/rocketgame.js',
    'rocket/js/rocketgame-customblockly.js'
]

var tutorials = [
    {
      'name': 'rocket-1',
      'blockly': true,
      'js': rocketGameJS,
      'libs' : easelBox2DLibs,
      'dir': 'rocket/',
      'xml' : 'rocket/xml/tutorial_1.xml',
      'html' : 'rocket/1.html'
    },
    {
      'name': 'rocket-2',
      'blockly': true,
      'js': rocketGameJS,
      'libs' : easelBox2DLibs,
      'dir': 'rocket/',
      'xml' : 'rocket/xml/tutorial_2.xml',
      'html' : 'rocket/1.html'
    },
    {
      'name': 'rocket-3',
      'blockly': true,
      'js': rocketGameJS,
      'libs' : easelBox2DLibs,
      'dir': 'rocket/',
      'xml' : 'rocket/xml/tutorial_3.xml',
      'html' : 'rocket/1.html'
    },
    {
      'name': 'rocket-4',
      'blockly': true,
      'js': rocketGameJS,
      'libs' : easelBox2DLibs,
      'dir': 'rocket/',
      'xml' : 'rocket/xml/tutorial_4.xml',
      'html' : 'rocket/1.html'
    },
    {
      'name': 'rocket-5',
      'blockly': false,
      'js': rocketGameJS,
      'dir': 'rocket/',
      'html' : 'rocket/1.html'
    }
];

// Create a server with a host and port
var server = new Hapi.Server();
server.connection({
    host: '0.0.0.0',
    port: 9500,
    routes: {
      cors: {
        origin: ['*'],
        credentials: 'true'
      }
    }
});

// Receive test data from the game
server.route({
    method: 'POST',
    path:'/test-data',
    handler: function (request, reply) {
       gResult = request.payload;
       console.log("GOT TEST DATA : " + request.payload);
       reply('ok');
    }
});

// Run the test
server.route({
    method: 'GET',
    path:'/test-result',
    handler: function (request, reply) {
        console.log("Request for /test-result");
        reply(gResult);
    }
});

// Index template serving
_.each(tutorials, function (tutorial) {
    var tutorialPath = '/' + tutorial.name;
    server.route({
        method: 'GET',
        path: tutorialPath,
        handler: function (req, reply) {
           try {
               var urlPath = url.parse(req.url).pathname;
               var query = url.parse(req.url, true).query;
               var libURL = query.test == true ? libURLTest : libURLProd;
               reply(indexTemplate({
                   isTest: query.test == true,
                   isBlockly: tutorial.blockly,
                   tutorialDirectory: tutorial.dir,
                   tutorialName: tutorial.name,
                   tutorialLibs: tutorial.libs,
                   tutorialJS: tutorial.js,
                   tutorialDefinition: JSON.stringify(tutorial, null, 2),
                   libURL: libURL,
                   libURLProd: libURLProd,
                   libURLTest: libURLTest
               }));
           }
           catch (e) {
               console.log(e);
               reply('URL Parameters error');
           }
        }
    });
});


// File serving
server.route({
    method: 'GET',
    path:'/{param*}',
    handler: function (req, reply) {
       var urlPath = url.parse(req.url).pathname;
       reply.file('.' + urlPath);
    }
});

// Start the server
server.start();

// Socket.io

var io = require('socket.io')(server.listener);

io.on('connection', function (socket) {
    console.log("Socket connection made");

    // games-test.js requests a reload of the test data
    socket.on('test-reload', function () {
        console.log("Got reload request...");
        // Send a reload command to the game
        io.emit('client-reload');
    });

    // Client has successfully reloaded and sent the data already
    socket.on('reloaded', function (data) {
        console.log("Client has reloaded and sent data ...");
        // Tell games-tests that the data is ready for evaluation
        io.emit('test-complete', data);
    });

    // Client error executing script, so abort
    socket.on('test-abort', function (data) {
        console.log("Client aborted test");
        // Tell games-tests about it
        io.emit('test-aborted', data);
    });

    socket.on('error', function(err) {
      console.log('Socket error: ' + err);
    });
});

io.on('error', function(err) {
  console.log('Socket.io error: ' + err);
});
