var request = require('request');
var io = require('socket.io-client');
var serverUrl = 'https://' + process.env.CODIO_BOX_DOMAIN + ':9500';
var gDebug = false;

function log(msg) {
  if(gDebug) {
    console.log(msg);
  }
} 

log("Main test request received");

// Deal with socket connection to hapi-server.js
var socket = io.connect(serverUrl);

socket.on('error', function(err) {
  log('Socket connection error: ' + JSON.stringify(err));
});

socket.on('disconnect', function(err) {
  log('Socket disconnected');
});

// Socket connection established with hapi-server, so request data from game
socket.on('connect', function() {
  
  // 'test-complete' event will be fired when data has been sent
  log('Socket connection made, requesting reload ...');
  socket.emit('test-reload');      
  
});

// Test data has been received, so return it
socket.on('test-complete', function() {
  log('test completed event receieved');
  getTestData();
});

// An error in the game, so abort the test
socket.on('test-aborted', function() {
  log('test aborted event receieved');
  console.log('Looks like your code has errors!');
  process.exit(1);
});

// Call the hapi server to get data
function getTestData() {
  
  request(serverUrl + '/test-result', function(error, response, body) {

    // Check for a system error such as Connection Refused
    if(error && response==undefined) {    
      log(error);
      console.log(error);  
      process.exit(2);
    }

    if (!error && response.statusCode == 200) {

      // HAPI Server returned OK
      if(body == '') {
        // No data returned by game, so tell user
        console.log("Make sure you press the Run button before you check your code.");  
        process.exit(1);            
      }

      // Call the actual data test
      res = JSON.parse(body);
      log(res); 

      // Call the test function as testIdn
      tests['t' + process.argv[2]](res);

    }
    else {

      // Some other error, so report
      if (response.statusCode == 502) {
        console.log("[" + response.statusCode + "] The service is not started. Please exit your project and re-open it. If you still see this error, please contact support@codio.com.")
      }  
      process.exit(2);

    }

  });
}

var tests = {

  t1 : function (res) {
    if(res.gravity == 10 && res.forceX == 50 && res.drag == 0) {
      console.log("Well done!!");
      process.exit(0);
    } else {
      console.log("Not done properly yet. You've got Gravity=" + res.gravity + " Horizontal Force=" + res.forceX + " Drag=" + res.drag + ".");
      process.exit(1);
    }
  }
  
}
  

